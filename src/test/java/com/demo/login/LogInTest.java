package com.demo.login;

import com.demo.base.BaseTest;
import com.demo.pages.Pages;
import com.demo.utils.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogInTest extends BaseTest {

    @Test
    public void verifyLogin() {

        Pages.loginPage().waitForLoginForm();
        Pages.loginPage().typeUserName(Constants.USERNAME);
        Pages.loginPage().typePassword(Constants.PASSWORD);
        Pages.loginPage().clickEnterButton();

    }
}