package com.demo.login;

import com.codeborne.selenide.Selenide;
import com.demo.action.Actions;
import com.demo.base.BaseTest;
import com.demo.object.Book;
import com.demo.pages.Pages;
import com.demo.utils.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class SearchAmazonBookTest extends BaseTest {

    List<Book> books;
    Book book;

    @Test
    public void searchBook() {
        Pages.searchFormAmazonPage().selectCategoryBooks();
        Pages.searchFormAmazonPage().setKeyWord();
        Pages.searchFormAmazonPage().submit();
        books = Actions.searchFormAmazonAction().findBooks();
        Selenide.open(Constants.AmazonBookURL);
        book = Actions.bookAction().findBook();
        Assert.assertTrue(Actions.searchFormAmazonAction().equalsBook(books,book));
    }
}
