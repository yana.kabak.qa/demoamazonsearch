package com.demo.login;

import com.codeborne.selenide.Selenide;
import com.demo.action.Actions;
import com.demo.base.BaseTest;
import com.demo.object.Book;
import com.demo.pages.Pages;
import com.demo.utils.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class SearchRussianAmazonBook extends BaseTest {

    List<Book> books;
    Book book;

    @Test
    public void searchRussianBook(){
        Pages.searchFormAmazonPage().selectCategoryBooks();
        Pages.searchFormAmazonPage().setKeyWord();
        Pages.searchFormAmazonPage().submit();
        Pages.searchFormAmazonPage().setFilterRussian();
        books = Actions.searchFormAmazonAction().findRussianBooks();
        Selenide.open(Constants.AmazonRussianBookPage);
        book = Actions.bookAction().findRussianBook();
        Assert.assertTrue(Actions.searchFormAmazonAction().equalsBook(books,book));
    }
}
