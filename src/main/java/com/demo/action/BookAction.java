package com.demo.action;

import com.demo.object.Book;
import com.demo.pages.Pages;

public class BookAction {
    public Book findBook(){
        Book book = new Book();
        book.setName(Pages.bookAmazonPage().setNameBook());
        book.setAuthor(Pages.bookAmazonPage().setAuthor());
        book.setBestseller(Pages.bookAmazonPage().setBestSeller());
        book.setPricePaper(Pages.bookAmazonPage().setPricePaper());
        return book;
    }

    public Book findRussianBook(){
        Book book = new Book();
        book.setName(Pages.bookAmazonPage().setNameBook());
        book.setAuthor(Pages.bookAmazonPage().setAuthor());
        book.setBestseller(Pages.bookAmazonPage().setBestSeller());
        book.setPricePaper(Pages.bookAmazonPage().setRussianBookPrice());
        return book;
    }
}
