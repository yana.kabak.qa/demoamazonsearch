package com.demo.action;

import com.demo.object.Book;
import com.demo.pages.Pages;
import com.demo.utils.StringFormUtil;

import java.util.ArrayList;
import java.util.List;

public class SearchFormAmazonAction {

    private List<Book> books;
    private String res;
    private String regPrice;
    private String id;

    public List<Book> findBooks(){
        books = new ArrayList<>();
        for (int i=0; i< Pages.searchFormAmazonPage().setArrayOfBookName().size(); i++) {
            Book book = new Book();
            book.setName(Pages.searchFormAmazonPage().setArrayOfBookName().get(i).getText());
            res = Pages.searchFormAmazonPage().setArrayOfAuthorName().get(i).getText();
            book.setAuthor(StringFormUtil.convertAuthor(res));
            regPrice = Pages.searchFormAmazonPage().setArrayOfPaperPrice().get(i).getText();
            book.setPricePaper(StringFormUtil.convertPrice(regPrice));
            id = StringFormUtil.convertStringID(Pages.searchFormAmazonPage().setIdOfBook());
            if(id.equals(Pages.searchFormAmazonPage().setArrayOfBooksId().get(i).getText())){
                book.setBestseller(true);
            }
            books.add(book);
        }

        return books;
    }

    public List<Book> findRussianBooks(){
        books = new ArrayList<>();
        for (int i=0; i<Pages.searchFormAmazonPage().setArrayOfBookName().size(); i++){
            Book book = new Book();
            book.setName(Pages.searchFormAmazonPage().setArrayOfBookName().get(i).getText());
            res = Pages.searchFormAmazonPage().setArrayOfAuthorName().get(i).getText();
            book.setAuthor(StringFormUtil.convertAuthor(res));
            regPrice = Pages.searchFormAmazonPage().setArrayOfPaperPrice().get(i).getText();
            book.setPricePaper(StringFormUtil.convertPrice(regPrice));
            book.setBestseller(false);
            books.add(book);
        }
        return books;
    }

    public boolean equalsBook(List<Book> books, Book book){
        for (Book value : books) {
            if (value.equals(book)) {
                return true;
            }
        }
        return false;
    }
}
