package com.demo.action;

public class Actions {

    private static SearchFormAmazonAction searchFormAmazonAction;
    private static BookAction bookAction;

    public static SearchFormAmazonAction searchFormAmazonAction() {
        if (searchFormAmazonAction == null) {
            searchFormAmazonAction = new SearchFormAmazonAction();
        }
        return searchFormAmazonAction;
    }

    public static BookAction bookAction() {
        if (bookAction == null) {
            bookAction = new BookAction();
        }
        return bookAction;
    }
}
