package com.demo.utils;

public class Constants {
    public static String URL = "https://www.google.com/";
    public static String USERNAME = "";
    public static String PASSWORD = "";
    public static String FIND = "by";
    public static String KeyWord = "Java";
    public static String BOOK_FILTER = "Books";
    public static String AmazonBookURL="https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/ref=sr_1_2?dchild=1&keywords=Java&qid=1610356790&s=books&sr=1-2";
    public static String AmazonManePage="https://www.amazon.com/";
    public static String AmazonRussianBookPage = "https://www.amazon.com/Java-взрослых-Russian-Alex-Nsky-ebook/dp/B098B8425K/ref=sr_1_1?dchild=1&keywords=java&qid=1625582495&refinements=p_n_feature_nine_browse-bin%3A3291441011&rnid=3291435011&s=books&sr=1-1";
}