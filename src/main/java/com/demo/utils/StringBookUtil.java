package com.demo.utils;

public class StringBookUtil {
    public static String convertAuthor(String author){
        author = author.substring(author.indexOf(Constants.FIND) + Constants.FIND.length());
        author = author.replaceAll(",","");
        author = author.replace("(Author)", "");
        author = author.replace("\n", "and ");
        author = author.substring(author.indexOf(" ")+1);
        return author;
    }
}
