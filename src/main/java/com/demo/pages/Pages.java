package com.demo.pages;

public class Pages {

    private static LoginPage loginPage;
    private static NavigationPage navigationPage;
    private static SearchFormAmazonPage searchFormAmazonPage;
    private static BookAmazonPage bookAmazonPage;

    public static LoginPage loginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage();
        }
        return loginPage;
    }

    public static NavigationPage navigationPage() {
        if (navigationPage == null) {
            navigationPage = new NavigationPage();
        }
        return navigationPage;
    }

    public static SearchFormAmazonPage searchFormAmazonPage(){
        if(searchFormAmazonPage == null) {
            searchFormAmazonPage = new SearchFormAmazonPage();
        }
        return searchFormAmazonPage;
    }

    public static BookAmazonPage bookAmazonPage(){
        if(bookAmazonPage == null){
            bookAmazonPage = new BookAmazonPage();
        }
        return bookAmazonPage;
    }
}