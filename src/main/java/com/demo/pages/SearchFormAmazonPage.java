package com.demo.pages;

import com.codeborne.selenide.SelenideElement;
import com.demo.base.BasePage;
import com.demo.utils.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SearchFormAmazonPage extends BasePage {
    private By tagName = By.tagName("select");
    private By name = By.name("field-keywords");
    private By nameButton = By.xpath("//*[@id='nav-search-submit-button']");
    private By booksName = By.xpath("//*[@class='sg-col-inner']//h2/a/span");
    private By booksAuthor = By.xpath("//*[@class='sg-col-inner']/div/div/div/div[@class='a-row']");
    private By bestSeller = By.xpath("//*[@class='a-badge-text']");
    private By booksPrice = By.xpath("//*[@class='a-section a-spacing-none a-spacing-top-small']");
    private By bookID1 = By.cssSelector("div[data-component-type='s-search-result']");
    private By bookFilter = By.xpath("//li[@id='p_n_feature_nine_browse-bin/3291441011']/span/a");

    public void setFilterRussian(){ click(bookFilter); }

    public void selectCategoryBooks(){
        Select select = new Select(getSelenideElement(tagName));
        select.selectByVisibleText(Constants.BOOK_FILTER);
    }

    public void setKeyWord(){ type(Constants.KeyWord, name); }

    public void submit(){ click(nameButton); }

    public List<SelenideElement> setArrayOfBookName(){
        return getElements(booksName); }

    public List<SelenideElement> setArrayOfAuthorName(){ return getElements(booksAuthor); }

    public List<SelenideElement> setArrayOfPaperPrice(){
        return getElements(booksPrice);
    }

    public String setIdOfBook(){
        if(!existsElement(bestSeller)){
            return getSelenideElement(bestSeller).getAttribute("id");
        }else {
            return "Elements are not best seller";
        }
    }

    public List<SelenideElement> setArrayOfBooksId(){
        return getElements(bookID1);
    }

    private boolean existsElement(By id) {
        try {
            getSelenideElement(id);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

}
