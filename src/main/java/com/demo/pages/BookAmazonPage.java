package com.demo.pages;

import com.demo.base.BasePage;
import com.demo.utils.StringBookUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class BookAmazonPage extends BasePage {
    private By bookName = By.xpath("//*[@id='productTitle']");
    private By bookAuthor = By.xpath("//*[@id='bylineInfo']");
    private By bookPrice = By.xpath("//*[@id='usedPrice']");
    private By russianBookPrice = By.xpath("//*[@id='kindle-price']");
    private By bestSeller = By.xpath("//*[@class='badge-link']/i");


    public String setNameBook(){ return getElementText(bookName); }

    public String setAuthor(){ return StringBookUtil.convertAuthor(getElementText(bookAuthor)); }

    public String setPricePaper(){ return getSelenideElement(bookPrice).getAttribute("data-asin"); }

    public String setRussianBookPrice() {return getSelenideElement(russianBookPrice).getAttribute("data-asin");}

    public boolean setBestSeller() {
        return !existsElement(bestSeller);
    }

    private boolean existsElement(By id) {
        try {
            getSelenideElement(id);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }
}
