package com.demo.pages;

import com.demo.base.BasePage;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    private By loginForm = By.className("loginForm");
    private By userNameField = By.name("login");
    private By passwordField = By.name("password");
    private By enterButton = By.xpath("");

    public void waitForLoginForm() {
        waitForElementVisibility(loginForm);
    }

    public void typeUserName(String email) {
        waitForElementVisibility(userNameField);
        type(email, userNameField);
    }

    public void typePassword(String password) {
        waitForElementVisibility(passwordField);
        type(password, passwordField);
    }

    public void clickEnterButton() {
        waitForElementVisibility(enterButton);
        click(enterButton);
    }

}