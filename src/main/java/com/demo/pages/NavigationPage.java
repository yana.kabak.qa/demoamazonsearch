package com.demo.pages;

import com.demo.base.BasePage;
import org.openqa.selenium.By;

public class NavigationPage extends BasePage {

    private By userName = By.xpath("");

    public String getUserName() {
        waitForElementVisibility(userName);
        return getElementText(userName);
    }
}